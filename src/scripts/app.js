document.addEventListener("DOMContentLoaded", function () {
    const list = [];

    const form = document.querySelector(".tourism-form");
    const imageInput = document.querySelector(".tourism-form-image-input");
    const titleInput = document.querySelector(".tourism-form-title-input");
    const descriptionInput = document.querySelector(".tourism-form-description-input");
    const items = document.querySelector(".tourism-points-grid");

    form.addEventListener("submit", addItemToList);

    /* let photo = document.getElementById("preview");
    let file = document.getElementById("item-image");

    photo.addEventListener("click", () => {
        if (file.isDefaultNamespace.length <= 0) {
            return;
        }

        let reader = new FileReader();

        reader.onload = () => {
            photo.src = reader.result;
        };

        document.getElementById("preview").style.visibility = "visible";
        reader.readAsDataURL(file.files[0]);
    }); */

    function addItemToList(event) {
        event.preventDefault();

        const reader = new FileReader();

        reader.onload = function () {
            const itemImage = reader.result;
            const itemTitle = event.target["item-name"].value;
            const itemDescription = event.target["description"].value;

            if (itemTitle != "" && itemDescription != "") {
                const item = {
                    image: itemImage,
                    title: itemTitle,
                    description: itemDescription,
                };

                list.push(item);
                renderListItems();
                resetInputs();
            } else {
                alert("Os campos não foram devidamente preenchidos.");
            }
        };

        reader.readAsDataURL(event.target["arquivo"].files[0]);
    }

    function renderListItems() {
        let itemsStructure = "";

        list.forEach(function (item) {
            itemsStructure += `
                <li class="tourism-point">
                    <img
                        class="tourism-point-image"
                        src="${item.image}"
                        alt="Imagem do Quarto Banner de Exemplo"
                    />
                    <div class="tourism-point-text">
                        <h2 class="tourism-point-title">${item.title}</h2>
                        <p class="tourism-point-description">
                            ${item.description}
                        </p>
                    </div>
                </li>
            `;
        });

        items.innerHTML = itemsStructure;
    }

    function resetInputs() {
        /*  imageInput.value = ""; */
        titleInput.value = "";
        descriptionInput.value = "";
        /*  document.getElementById("preview").style.visibility = "hidden"; */
    }
});
